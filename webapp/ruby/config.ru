require './app'

require 'dotenv'
Dotenv.load

if ENV['STACKPROF'] == '1'
  puts 'enable stackprof'
  require 'stackprof'
  Dir.mkdir('/tmp/stackprof') unless File.exist?('/tmp/stackprof')
  use StackProf::Middleware, enabled: true,
	mode: :wall,
    interval: 100,
    save_every: 1,
    path: '/tmp/stackprof'
end

run App
